﻿using System.Net.Http;

namespace AAdharClient
{
    public class AAdhaarGatewayClient : IAAdharGatewayClient
    {
        readonly string _authUrl;
        readonly string _otpUrl;
        readonly string _deviceId;
        readonly CertiticateType _certiticateType;
        readonly Location _location;
        readonly int _matchingValue;

        public AAdhaarGatewayClient(
            string authUrl,
            string otpUrl,
            CertiticateType certificateType,
            Location location
            )
        {
            this._authUrl = authUrl;
            this._otpUrl = otpUrl;
            this._certiticateType = certificateType;
            this._deviceId = "public";
            this._location = location;
            this._matchingValue = 80;
        }

        public AAdhaarGatewayClient(
            string authUrl,
            string otpUrl,
            CertiticateType certificateType,
            string pinCode
            )
            : this(
            authUrl,
            otpUrl,
            certificateType,
            new Location()
            {
                Type = Location.LocationType.pincode,
                Pincode = pinCode
            }
            )
        {

        }

        public AuthResponse InitiateOTPRequest(string aadharId, OTPRequest.OTPChannel channel)
        {
            var otpRequest = new OTPRequest()
            {
                AAdhaarID = aadharId,
                DevideId = this._deviceId,
                CertificateType = this._certiticateType,
                Channel = channel,
                Location = this._location
            };

            var authResponse = HttpHelper.GetResponse<OTPRequest, AuthResponse>(HttpMethod.Post, this._otpUrl, otpRequest);
            return authResponse;
        }

        public AuthResponse AuthenticateAadharId(AuthRequest authRequest)
        {
            var authResponse = HttpHelper.GetResponse<AuthRequest, AuthResponse>(HttpMethod.Post, this._authUrl, authRequest);
            return authResponse;
        }

        public AuthResponse AuthenticateAadharId(string aadharId, string name, string email, string phoneNumber)
        {
            return AuthenticateAadharId(aadharId, name, email, phoneNumber, null);
        }

        public AuthResponse AuthenticateAadharId(string aadharId, string name, string email, string phoneNumber, Demographics.AddressStructured structuredAddress)
        {
            var authRequest = new AuthRequest()
            {
                AAdharID = aadharId,
                DeviceId = _deviceId,
                Modality = AAdharClient.Modality.demo,
                CertificateType = _certiticateType,
                Location = this._location,
                Demographics = new Demographics()
                {
                    Name = new Demographics.NameStructure()
                    {
                        MatchingStrategy = MatchingStrategy.partial,
                        MatchingValue = this._matchingValue,
                        NameValue = name
                    },
                    Email = email,
                    Phone = phoneNumber,
                    AddressStructuredValue = structuredAddress
                }
            };
            return AuthenticateAadharId(authRequest);
        }


        public AuthResponse AuthenticateAadharId(string aadharId, string otp)
        {
            var authRequest = new AuthRequest()
            {
                AAdharID = aadharId,
                DeviceId = _deviceId,
                Modality = AAdharClient.Modality.otp,
                CertificateType = _certiticateType,
                OTP = otp,
                Location = _location
            };
            return AuthenticateAadharId(authRequest);
        }
    }
}
