﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;

namespace AAdharClient
{
    public enum CertiticateType
    {
        prod,
        preprod
    }

    public enum Modality
    {
        biometric,
        demo,
        otp
    }

    public enum ModalityType
    {
        fp,
        iris
    }

    public class AuthRequest
    {
        [JsonProperty("aadhaar-id")]
        public string AAdharID { get; set; }

        [JsonProperty("modality")]
        [JsonConverter(typeof(StringEnumConverter))]
        public Modality Modality { get; set; }

        [JsonProperty("modality-type")]
        [JsonConverter(typeof(StringEnumConverter))]
        public ModalityType? ModalityType { get; set; }

        [JsonProperty("number-of-fingers-to-capture")]
        public int? NumberOfFingersToCapture { get; set; }
        
        [JsonProperty("number-of-iris-to-cature")]
        public int? NumberOfIrisToCapture { get; set; }

        [JsonProperty("otp")]
        public string OTP { get; set; }

        [JsonProperty("device-id")]
        public string DeviceId { get; set; }

        [JsonProperty("certificate-type")]
        [JsonConverter(typeof(StringEnumConverter))]
        public CertiticateType CertificateType { get; set; }

        [JsonProperty("demographics")]
        public Demographics Demographics { get; set; }

        [JsonProperty("location")]
        public Location Location { get; set; }
    }

    public class Demographics
    {
        [JsonProperty("name")]
        public NameStructure Name { get; set; }

        [JsonProperty("dob")]
        public DOB DOB { get; set; }

        [JsonProperty("phone")]
        public string Phone { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("gender")]
        [JsonConverter(typeof(StringEnumConverter))]
        public Gender GenderValue{ get;set;}

        [JsonProperty("address-format")]
        [JsonConverter(typeof(StringEnumConverter))]
        public AddressFormat? AddressFormatValue { get; set; }

        [JsonProperty("address-freetext")]
        public AddressFreeText AddressFreeTextValue { get; set; }

        [JsonProperty("address-structured")]
        public AddressStructured AddressStructuredValue { get; set; }

        public enum AddressFormat
        {
            freetext,
            structured
        }

        public class NameStructure
        {
            [JsonProperty("matching-strategy")]
            [JsonConverter(typeof(StringEnumConverter))]
            public MatchingStrategy MatchingStrategy { get; set; }

            [JsonProperty("matching-value")]
            public int MatchingValue { get; set; }

            [JsonProperty("name-value")]
            public string NameValue { get; set; }
        }

        public class AddressStructured
        {
            //TODO:- Not complete

            [JsonProperty("pincode")]
            public string Pincode { get; set; }

            [JsonProperty("state")]
            public string State { get; set; }

            [JsonProperty("district")]
            public string District { get; set; }

            [JsonProperty("subdistrict")]
            public string SubDistrict { get; set; }

            [JsonProperty("street")]
            public string Street { get; set; }

        }

        public class AddressFreeText
        {
            [JsonProperty("matching-strategy")]
            public MatchingStrategy MatchingStrategy { get; set; }

            [JsonProperty("matching-value")]
            public int MatchingValue { get; set; }

            [JsonProperty("address-value")]
            public string AddressValue { get; set; }
        }

        public enum Gender
        {
            male,
            female,
            transgender
        }
    }

    public class Location
    {
        [JsonProperty("pincode")]
        public string Pincode { get; set; }

        [JsonProperty("type")]
        [JsonConverter(typeof(StringEnumConverter))]
        public LocationType Type { get; set; }

        public enum LocationType
        {
            pincode
        }
    }

    public enum MatchingStrategy
    {
        partial
    }

    public class DOB
    {
        [JsonProperty("dob-value")]
        public string DobValue { get; set; }

        public void SetDobValueFromDate(DateTime dob)
        {
            this.DobValue = dob.ToString("yyyymmdd");
        }
    }
}
