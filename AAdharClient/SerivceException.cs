﻿using System;
using System.Net;

namespace AAdharClient
{
    public class ServiceException : Exception
    {
        public HttpStatusCode StatusCode { get; private set; }
        public string Description { get; private set; }

        public ServiceException(string message, HttpStatusCode statusCode, string description,Exception e):base(message,e)
        {
            this.StatusCode = statusCode;
            this.Description = description;
        }
    }
}
