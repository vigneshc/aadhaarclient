﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace AAdharClient
{
    public class OTPRequest
    {
        [JsonProperty("aadhaar-id")]
        public string AAdhaarID { get; set; }

        [JsonProperty("device-id")]
        public string DevideId { get; set; }

        [JsonProperty("certificate-type")]
        [JsonConverter(typeof(StringEnumConverter))]
        public CertiticateType CertificateType { get; set; }

        [JsonProperty("channel")]
        [JsonConverter(typeof(StringEnumConverter))]
        public OTPChannel Channel { get; set; }

        [JsonProperty("location")]
        public Location Location { get; set; }

        public enum OTPChannel
        {
            SMS,
            EMAIL,
            EMAIL_AND_SMS
        }
    }
}
