﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Web;
using System.IO;
using Newtonsoft.Json;

namespace AAdharClient
{
    static class HttpHelper
    {
        public static ResponseBodyType GetResponse<RequestBodyType, ResponseBodyType>(HttpMethod method, string url, RequestBodyType requestBody)
        {
            var httpRequest = (HttpWebRequest)WebRequest.Create(url);
            httpRequest.Method = method.ToString();
            httpRequest.ContentType = "application/json";

            using (var requestStream = httpRequest.GetRequestStream())
            {
                using (var streamWriter = new StreamWriter(requestStream))
                {
                    var requestBodyString = JsonConvert.SerializeObject(requestBody, Formatting.Indented);
                    streamWriter.Write(requestBodyString);
                }
            }

            using (var response = (HttpWebResponse)httpRequest.GetResponse())
            {
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new ServiceException("UnExpectedStatusCode", response.StatusCode, response.StatusDescription, null);
                }

                using (var responseStream = response.GetResponseStream())
                {
                    using (var streamReader = new StreamReader(responseStream))
                    {
                        var responseString = streamReader.ReadToEnd();
                        return JsonConvert.DeserializeObject<ResponseBodyType>(responseString);
                    }
                }
            }
        }
    }
}
