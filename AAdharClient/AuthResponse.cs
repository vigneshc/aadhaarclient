﻿using Newtonsoft.Json;

namespace AAdharClient
{
    //http://bridge.aadhaarconnect.com/gatewaysdk.html

    public class AuthResponse
    {
        [JsonProperty("aadhaar-id")]
        public string AAdhaarID { get; set; }

        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("aadhaar-reference-code")]
        public string ReferenceCode { get; set; }

        [JsonProperty("aadhaar-status-code")]
        public string StatusCode { get; set; }
    }
}
