﻿namespace AAdharClient
{
    interface IAAdharGatewayClient
    {
        AuthResponse InitiateOTPRequest(string aadharId, OTPRequest.OTPChannel channel);
        AuthResponse AuthenticateAadharId(AuthRequest authRequest);
        AuthResponse AuthenticateAadharId(string aadharId, string name, string email, string phoneNumber);
        AuthResponse AuthenticateAadharId(string aadharId, string name, string email, string phoneNumber, Demographics.AddressStructured structuredAddress);
        AuthResponse AuthenticateAadharId(string aadharId, string otp);
    }
}
