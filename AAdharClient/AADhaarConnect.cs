﻿namespace AAdharClient
{
    public static class AADhaarConnect
    {
        public static AAdhaarGatewayClient GetPreProdHackathonClient(Location location)
        {
            var aadhaarClient = new AAdhaarGatewayClient(
                "https://ac.khoslalabs.com/hackgate/hackathon/auth/raw",
                "https://ac.khoslalabs.com/hackgate/hackathon/otp",
                CertiticateType.preprod,
                location
                );
            return aadhaarClient;
        }
    }
}
