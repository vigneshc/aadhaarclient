﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AAdharClient;

namespace AAdharClientTests
{
    [TestClass]
    public class AAdhaarGatewayClientTest
    {
        /*Validation succeeds only with real aadhar number. These tests mostly validate that we get validation failure
         * Data for this comes from below page.
         * https://developer.uidai.gov.in/site/node/21
         */

        static AAdhaarGatewayClient _aadharClient;

        [ClassInitialize]
        public static void ClassInitialize(TestContext testcontext)
        {
            _aadharClient = AADhaarConnect.GetPreProdHackathonClient(new Location()
                {
                    Type = Location.LocationType.pincode,
                    Pincode = "486441"
                });
        }
        
        [TestMethod]
        [TestCategory("RequireRealData")]
        public void BasicDemoAuthTest()
        {
            var authResponse = _aadharClient.AuthenticateAadharId("999999990026", "Kumar Agarwal", null, null);
            Assert.IsFalse(authResponse.Success);
            Assert.AreEqual("998", authResponse.StatusCode);//invalid aadharnumber
        }

        [TestMethod]
        [TestCategory("RequireRealData")]
        public void BasicOTPAuthTest()
        {
            var otp = "123456";
            var authResponse = _aadharClient.AuthenticateAadharId("999999990026", otp);
            Assert.IsFalse(authResponse.Success);
            Assert.AreEqual("998", authResponse.StatusCode);//invalid 
        }

        [TestMethod]
        [TestCategory("RequireRealData")]
        public void BasicOTPRequestTest()
        {
            var authResponse = _aadharClient.InitiateOTPRequest("999999990026",OTPRequest.OTPChannel.EMAIL_AND_SMS);
            Assert.IsFalse(authResponse.Success);
            Assert.AreEqual("998", authResponse.StatusCode);//invalid 
        }
    }
}
